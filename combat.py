import math
import random
import sys
import operator


if __name__ == "__main__":
    for j in range(21 - 5):
        confidence = j / 20.0 + 0.25
        nb_correct = 0
        nb_wrong = 0
        score_fou = 0
        score_balance = 0
        for i in range(1000):
            result = random.uniform(0.0, 1.0)
            if result <= confidence:
                # Our best guess was the correct one
                score_fou += 1
                score_balance += confidence - ((1 - confidence) / 3) * 3 / 3
                nb_correct += 1
            else:
                score_fou += -1 / 3
                score_balance += (
                    -confidence / 3
                    - 2 * ((1 - confidence) / 3) / 3
                    + ((1 - confidence) / 3)
                )
                # score_balance = (1 - 5 * confidence) / 9
                nb_wrong += 1
        print(
            "confidence={}, score_fou={}, score_balance={}, diff={}, nb_correct={}, nb_wrong={}".format(
                confidence,
                score_fou,
                score_balance,
                score_fou - score_balance,
                nb_correct,
                nb_wrong,
            )
        )
