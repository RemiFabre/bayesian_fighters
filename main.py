import math
import random
import sys
import operator


class reliable_student:
    def __init__(self, nb_choices, list_of_methods):
        # This is the probability that the student knows the correct answer.
        # In this simulation, the student has a perfect knowledge of his own knowledge.
        # Also, the student's knowledge will never be lower than 1.0/nb_choices
        # (which is equivalent to not knowing anything but at least not "knowing" something that is fals
        # e).
        # This reminds me of John Snow. Changing the name of the class to "reliable_student" to highlight how proud I am that these students
        # dont bullshit their knowledge.
        # Also, the student knows how the MCQ will be scored and always uses the best strategy.
        # e.g on "chevre0" the best strategy is to bet everything on your best answer,
        # on "quadratic" the best strategy is to bet honestly (proportionately to the knowledge).
        self.knowledge = random.uniform(1.0 / nb_choices, 1.0)
        self.nb_choices = nb_choices
        self.marks = {}
        for method in list_of_methods:
            self.marks[method] = 0

    def score_all_methods(self, luck):
        for method, _ in self.marks.items():
            self.score_question(method, luck)

    def score_question(self, method, luck):
        if method == "quadratic":
            # The best strategy is to bet accordingly to our knowledge
            # So knowledge on our best answer, and the rest evenly distributed
            # The best answer will be correct knowledge % of times
            # This is the value the student answered on the other answers
            secondary_answer = (1 - self.knowledge) / (self.nb_choices - 1)
            if self.knowledge >= luck:
                # Our best answer was the correct one
                score = 1 - (
                    math.pow((1 - self.knowledge), 2)
                    + (self.nb_choices - 1) * math.pow(secondary_answer, 2)
                )
            else:
                # Our best answer wans't the correct one
                score = 1 - (
                    math.pow(1 - secondary_answer, 2)
                    + math.pow(self.knowledge, 2)
                    + (self.nb_choices - 2) * math.pow(secondary_answer, 2)
                )
        elif method == "quadratic_polarized":
            # When we know, we're sure it's correct
            if self.knowledge >= luck:
                # Our best answer was the correct one
                score = 1
            else:
                # Jon snow
                secondary_answer = 1.0 / (self.nb_choices)
                score = 1 - (
                    math.pow(1 - secondary_answer, 2)
                    + (self.nb_choices - 1) * math.pow(secondary_answer, 2)
                )
        elif method == "hairyWiseMonkey":
            # Proposed by cduss because it's "perfect"
            secondary_answer = (1 - self.knowledge) / (self.nb_choices - 1)
            if self.knowledge >= luck:
                # Our best answer was the correct one
                score = 2 * self.knowledge - (self.nb_choices - 1) * math.pow(
                    secondary_answer + 0.5, 4
                )
            else:
                score = (
                    2 * secondary_answer
                    - (self.nb_choices - 2) * math.pow(secondary_answer + 0.5, 4)
                    - math.pow(self.knowledge + 0.5, 4)
                )

        elif method == "chevre0":
            # The best strategy is to bet everything on the best answer
            if self.knowledge >= luck:
                # Our best answer was the correct one
                score = 1
            else:
                # Our best answer wasn't the correct one
                score = -1.0 / (self.nb_choices - 1)
        elif method == "classic":
            # The best strategy is to bet everything on the best answer
            if self.knowledge >= luck:
                # Our best answer was the correct one
                score = 1
            else:
                # Our best answer wasn't the correct one
                score = 0
        else:
            print("Error: unknown method '{}'. Leaving.".format(method))
            sys.exit()
        # Addind the points to the final mark
        self.marks[method] += score

    def __repr__(self):
        result = "Student knowledge:{}".format(self.knowledge)
        for method, mark in self.marks.items():
            result += "\n***Mark with method '{}': {}".format(method, mark)
        result += "\n"
        return result


def team_score(team):
    score = 0
    for student in team:
        score += student.knowledge
    return score


def fight(
    methods, nb_choices=4, nb_questions=20, nb_total_candidates=100, team_size=10
):
    candidates = []
    teams_scores = {}
    for i in range(nb_total_candidates):
        candidates.append(reliable_student(nb_choices, methods))
    # print("method {}, candidates \n{}".format("None", candidates))

    # The goal is to compare the efficiency of different scoring methods.
    # Each student will answer nb_questions x nb_scoring_methods.
    # However, the questions are the same regardless of the scoring method
    for question in range(nb_questions):
        # How hard the question is
        luck = random.uniform(1.0 / nb_choices, 1)
        for student in candidates:
            student.score_all_methods(luck)
    # print(candidates)
    # Chosing the best team according to each method
    best_teams = {}
    for method in methods:
        # This shuffle will randomize the impact of ties which adds bias to the final score
        random.shuffle(candidates)
        local_candidates = sorted(
            candidates, key=lambda x: x.marks[method], reverse=True
        )
        best_team = local_candidates[0:team_size]
        # print("method {}, candidates \n{}".format(method, local_candidates))
        best_teams[method] = best_team
        teams_scores[method] = team_score(best_team)
    # print(teams_scores)
    # Checking what the best possible team would be
    candidates.sort(key=lambda x: x.knowledge, reverse=True)
    best_team = candidates[0:team_size]
    best_team_score = team_score(best_team)
    # print(candidates)

    # print(
    #     "The best possible team scores {}, with students:\n{}".format(
    #         best_team_score, best_team
    #     )
    # )
    sorted_scores = sorted(
        teams_scores.items(), key=operator.itemgetter(1), reverse=True
    )
    return sorted_scores, best_team_score


if __name__ == "__main__":
    # methods = ["quadratic", "chevre0"]
    methods = [
        "quadratic",
        "chevre0",
        "classic",
        "quadratic_polarized",
        "hairyWiseMonkey",
    ]
    nb_fights = 100
    scores = {}
    local_reward = {}
    for method in methods:
        scores[method] = 0
        local_reward[method] = 0
    for i in range(nb_fights):
        sorted_scores, best_team_score = fight(
            methods,
            nb_choices=4,
            nb_questions=10,
            nb_total_candidates=100,
            team_size=10,
        )
        print("{}. Max available was: {}".format(sorted_scores, best_team_score))
        for j in range(len(sorted_scores)):
            points = len(methods) - 1 - j
            method = sorted_scores[j][0]
            local_reward[method] = points
            if j > 0:
                # Tie management. All ties get the points of the best available spot
                if math.isclose(sorted_scores[j][1], sorted_scores[j - 1][1]):
                    # Giving the same amount than the "higher" tie
                    points = local_reward[sorted_scores[j - 1][0]]
                    local_reward[method] = points
            scores[method] += local_reward[method]
        # print("Local reward {}".format(local_reward))
        # if math.isclose(sorted_scores[0][1], sorted_scores[1][1]):
        #     print("Tie!")
        # else:
        #     print("The best method was {}".format(sorted_scores[0][0]))

    print("\n\nFinal results:")
    scores = sorted(scores.items(), key=operator.itemgetter(1), reverse=True)
    print(scores)
    print("Maximum final score is {}".format(nb_fights * (len(methods) - 1)))
